//
//  ViewController.swift
//  Multiscreen
//
//  Created by Markus Stöbe on 30.04.17.
//  Copyright © 2017 Markus Stöbe. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	var center = NotificationCenter.default;
	var secondWindow : UIWindow?
	var secondBaseView : UIView?

	var externalLabel = UILabel()

	override func viewDidLoad() {
		super.viewDidLoad()

		center.addObserver(self,
		                   selector: (#selector(ViewController.setupScreens)),
		                   name: NSNotification.Name.UIScreenDidConnect,
		                   object: nil)

		self.setupScreens()
	}

	override func viewWillDisappear(_ animated: Bool) {
		center.removeObserver(self)
	}

	func setupScreens() {
		if (UIScreen.screens.count > 1) {
			//if there are more than 1 screen present, use them
			let secondScreen = UIScreen.screens[1]
			secondWindow = UIWindow(frame: secondScreen.bounds)

			let viewController = UIViewController()
			secondWindow?.rootViewController = viewController
			secondWindow?.screen = secondScreen

			secondBaseView = UIView(frame: secondWindow!.frame)
			secondBaseView?.backgroundColor = UIColor.green
			secondWindow?.addSubview(secondBaseView!)
			secondWindow?.isHidden = false


			externalLabel.frame = (secondBaseView?.frame)!
			externalLabel.text = "Hello TV!"
			externalLabel.font = UIFont(name: "Helvetica", size: 50.0)
			externalLabel.textAlignment = NSTextAlignment.center
			secondBaseView?.addSubview(externalLabel)
		}
	}
}

