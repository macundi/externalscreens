//
//  NewsItem.m
//  News
//
//  Created by Markus Stöbe on 28.10.15.
//  Copyright © 2015 Markus Stöbe. All rights reserved.
//

#import "NewsItem.h"

@implementation NewsItem

@synthesize date = _date;

-(instancetype)init {
    self = [super init];
    if (self) {
        self.title    = [[NSMutableString alloc] init];
        self.link     = [[NSMutableString alloc] init];
        self.summary  = [[NSMutableString alloc] init];
        self.imageURL = [[NSMutableString alloc] init];
        self.date     = [[NSMutableString alloc] init];
    }
    return self;
}

-(void)setDate:(NSString *)date {
    _date = [date mutableCopy];
}

- (NSString *)date {
    //create dateFormatter that matches incoming format
    //2015-10-28T07:33:00+1:00
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale     = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    
    NSDate *thisDate = [dateFormatter dateFromString:_date];
    return thisDate ? [NSDateFormatter localizedStringFromDate:thisDate dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterMediumStyle]
                    : _date;
}

@end
