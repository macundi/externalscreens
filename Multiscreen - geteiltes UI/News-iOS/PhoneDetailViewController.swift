//
//  DetailViewController.swift
//  News-iOS
//
//  Created by Markus Stöbe on 24.08.16.
//  Copyright © 2016 Markus Stöbe. All rights reserved.
//

import UIKit

class PhoneDetailViewController: UIViewController {

	@IBOutlet weak var detailDescriptionLabel: UILabel!
	@IBOutlet weak var image: UIImageView!


	func configureView() {
		// Update the user interface for the detail item.
		if let detail = self.detailItem {
		    if let label = self.detailDescriptionLabel {
		        label.text = detail.title

				let url       = URL(string:detail.imageURL)
				let imageData = NSData(contentsOf: url!)
				let image     = UIImage(data: imageData! as Data)
				self.image.image    = image!
		    }
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		self.configureView()
	}

	var detailItem: NewsItem? {
		didSet {
		    // Update the view.
		    self.configureView()
		}
	}


}

