//
//  MousePadViewController.swift
//  News
//
//  Created by Markus Stöbe on 04.06.17.
//  Copyright © 2017 Markus Stöbe. All rights reserved.
//

import UIKit

protocol MousePadProtocol {
	func mouseBegan(_ touches: Set<UITouch>, with event: UIEvent?)
	func mouseMoved(_ touches: Set<UITouch>, with event: UIEvent?)
	func mouseEnded(_ touches: Set<UITouch>, with event: UIEvent?)
}

class MousePadViewController: UIViewController {

	var delegate: MousePadProtocol?

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		if delegate != nil {
			delegate?.mouseBegan(touches, with: event)
		}
	}

	override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
		if delegate != nil {
			delegate?.mouseMoved(touches, with: event)
		}
	}

	override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
		if delegate != nil {
			delegate?.mouseEnded(touches, with: event)
		}
	}

}
