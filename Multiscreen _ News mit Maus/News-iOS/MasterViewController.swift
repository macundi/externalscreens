//
//  MasterViewController.swift
//  News-iOS
//
//  Created by Markus Stöbe on 24.08.16.
//  Copyright © 2016 Markus Stöbe. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController, RssDelegate, MousePadProtocol {

	//******************************************************************************************************************
	//* MARK: - Vars and Outlets
	//******************************************************************************************************************
	var detailViewController: PhoneDetailViewController? = nil
	var objects = [Any]()
	var rssparser: RssParser? = nil

	//…for external Display
	var center = NotificationCenter.default
	var secondScreenPresent = false
	var secondWindow : UIWindow?
	var externalViewController: MousePadViewController? = nil
	var pointer = UIImageView()
	var xratio : CGFloat = 0.0, yratio : CGFloat = 0.0

	//******************************************************************************************************************
	//* MARK: - Lifecycle
	//******************************************************************************************************************
	override func viewDidLoad() {

		super.viewDidLoad()

		// Do any additional setup after loading the view, typically from a nib.
		if let split = self.splitViewController {
		    let controllers = split.viewControllers
		    self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? PhoneDetailViewController
		}
		
		self.rssparser = RssParser.init(delegate: self)
	}

	override func viewWillAppear(_ animated: Bool) {
		self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
		super.viewWillAppear(animated)
		self.rssparser?.loadNews()

		//signup for notifications
		center.addObserver(self, selector:#selector(MasterViewController.setupScreens), name: NSNotification.Name.UIScreenDidConnect, object: nil)
		center.addObserver(self, selector:#selector(MasterViewController.closeScreen),  name: NSNotification.Name.UIScreenDidDisconnect, object: nil)

		//setup external screen if available
		self.setupScreens()
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		center.removeObserver(self)
	}

	//******************************************************************************************************************
	//* MARK: - External Screen
	//******************************************************************************************************************
	func setupScreens() {
		if (UIScreen.screens.count > 1) {

			let mainWindow = UIApplication.shared.keyWindow
			mainWindow?.isHidden = true

			pointer.image = UIImage(named: "pointer")
			pointer.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
			pointer.isHidden = true
			mainWindow?.rootViewController?.view.addSubview(pointer)

			//if there is more than 1 screen present, use it
			let secondScreen = UIScreen.screens[1]
			mainWindow?.screen = secondScreen	//move mainWindow to secondscreen
			mainWindow?.frame = secondScreen.bounds
			mainWindow?.isHidden = false

			externalViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MousePad") as? MousePadViewController
			externalViewController?.delegate = self;
			secondWindow = UIWindow(frame: UIScreen.main.bounds)
			secondWindow?.rootViewController = externalViewController
			secondWindow?.screen   = UIScreen.main

			xratio = secondScreen.bounds.width/UIScreen.main.bounds.width
			yratio = secondScreen.bounds.height/UIScreen.main.bounds.height

			secondWindow?.isHidden = false
			secondScreenPresent = true
		}
	}

	func closeScreen() {
		let mainWindow = UIApplication.shared.keyWindow
		mainWindow?.isHidden = true
		secondWindow?.isHidden = true

		mainWindow?.screen = UIScreen.main
		mainWindow?.frame  = UIScreen.main.bounds
		mainWindow?.isHidden = false

		secondScreenPresent = false
	}

	func mouseBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		self.pointer.isHidden = false
	}

	func mouseMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
		for touch in touches {
			var pointOfContact = touch.location(in: self.view)
			pointOfContact.x   = pointOfContact.x * xratio
			pointOfContact.y   = pointOfContact.y * yratio
			pointer.center     = pointOfContact
		}
	}

	func mouseEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
		self.pointer.isHidden = true

		for touch in touches {
			var pointOfContact = touch.location(in: self.view)
			pointOfContact.x   = pointOfContact.x * xratio
			pointOfContact.y   = pointOfContact.y * yratio

			let index = self.tableView.indexPathForRow(at: pointOfContact)
			self.tableView.selectRow(at: index, animated: true, scrollPosition: .middle)
			self.performSegue(withIdentifier: "showDetail", sender: self)
		}
	}
	//******************************************************************************************************************
	//* MARK: - RssParserDelegate
	//******************************************************************************************************************
	func rssParser(_ parser: RssParser!, didFinishParsingAndFoundNews foundStories: [Any]!) {
		print("I found \(foundStories.count) stories at Mac & i ")
		self.objects = foundStories
		self.tableView.reloadData()
	}

	//******************************************************************************************************************
	//* MARK: - Segues
	//******************************************************************************************************************
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "showDetail" {
		    if let indexPath = self.tableView.indexPathForSelectedRow {
		        let object = objects[indexPath.row] as! NewsItem
		        let controller = (segue.destination as! UINavigationController).topViewController as! PhoneDetailViewController
		        controller.detailItem = object
		        controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
		        controller.navigationItem.leftItemsSupplementBackButton = true
		    }
		}
	}

	//******************************************************************************************************************
	//* MARK: - Tableview
	//******************************************************************************************************************
	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return objects.count
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

		let object = objects[indexPath.row] as! NewsItem
		cell.textLabel!.text		= object.title
		cell.detailTextLabel!.text	= object.summary
		return cell
	}
}

